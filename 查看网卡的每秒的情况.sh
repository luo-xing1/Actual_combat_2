#!/bin/bash
echo -e "IN ---------------Out"

for NIC in "$@"; do
    echo "Monitoring interface: $NIC"
    while true; do
        ens_in=$(awk -v nic="$NIC" '$0 ~ nic {print $2}' /proc/net/dev)
        ens_out=$(awk -v nic="$NIC" '$0 ~ nic {print $10}' /proc/net/dev)
        sleep 5
        new_ens_in=$(awk -v nic="$NIC" '$0 ~ nic {print $2}' /proc/net/dev)
        new_ens_out=$(awk -v nic="$NIC" '$0 ~ nic {print $10}' /proc/net/dev)
        in=$(printf "%.1f%s" "$((($new_ens_in-$ens_in)/1024))" "KB/s")
        out=$(printf "%.1f%s" "$((($new_ens_out-$ens_out)/1024))" "KB/s")
        echo "$NIC: $in $out"
        sleep 1
    done
done