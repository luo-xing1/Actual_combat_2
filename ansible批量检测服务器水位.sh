#!/bin/bash
#利用ansible 检测硬盘水位
ansible_path(){
echo "-------------------查看磁盘占用率------------------------------------------------"
ansible all -m shell -a "df -h" >df.txt #写入到文本里面，我这里是单词写入。
cat df.txt |awk '/\/$/{print $5}' #这里可以查看到机器里面的占用情况。
echo "-------------------查看内存占用率------------------------------------------------"
ansible all -m shell -a "echo $(free -m |awk 'NR==2{print $3/$2*100}')%" #查询机器的可用内存。
echo "-------------------查看CPU占用率------------------------------------------------"
ansible all -m shell -a "echo $(sar -u 1 1|awk 'NR==5{print $NF}')%" #查询机器的CPU
echo "-------------------查看显卡占用率------------------------------------------------"
ansible all -m shell -a "echo $(nvidia-smi |awk 'NR==10'| awk -F ' ' '{printf "%.1f", $9 /$11*100}')%" #查询显卡占用率。
}
ansible_path