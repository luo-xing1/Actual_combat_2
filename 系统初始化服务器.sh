#!/bin/bash
# @Time      :2023/12/18 20:23
# @Author    :Rohing
#设置同步时间
ln -s /usr/share/zoneinfo/Asia/Shanghai /etc/localtimme
   if ! crotab -l |grep ntpdate &>/dev/null ;then
         (echo "* 1 * * * ntpdate time.aliyun.com >/dev/null 2>&1";crotab -l) |crotab   fi
#禁用SELINUX
sed -i '/SELINUX/{s/permissive/disabled/}' /etc/selinux/config
#关闭防火墙
systemctl stop firwall ||systemctl stop ufw
#显示历史命令
    if ! grep HISTIMEFORMAT /etv/bashrc ;then
        echo 'export HISTIMEFORMAT=%F%T `whoami` "' >>/etc/bashrc
    fi
#ssh超时时间
  if ! grep "TMOUT=600" /etc/profile &>/dev/null ;then
       echo "export TMOUT=600" >>/etc/profile
  fi
#禁止root远程登录
sed -i 's/#PermintRootLogin yes/#PermintRootLogin no' /etc/ssh /sshd_config
#禁止定时任务发送邮件
sed -i 's/^MAILTO=root/MAILTO=""/' /etc/crontab
#设置最大打开文件数
if ! grep "* soft nofile 65535" /etc/security/limits.conf &>/dev/null ;then
cat >>/etc/security/limits.conf <<EOF
  * soft nofile 65535
  * hard nofile 65535
EOF
fi
#系统内核优化
cat     /etc/sysctl.conf <<EOF
net.ipv4.tcp_syncookies = 1
net.ipv4.tcp_max_tw_buckets =20480
net.ipv4.tcp_max_syn_backlog =20480
net.core.netdev_max_backlog = 262144
net.ipv4.tcp_fin_timeout = 20
EOF
#减少SAWP使用
echo "0" >/proc/sys/vm/swappiness